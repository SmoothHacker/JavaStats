import java.util.Scanner;
import java.lang.Math;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.commons.math3.distribution.NormalDistribution;

public class JavaStats {
  public static void main(String[] args) {
    Scanner inpt = new Scanner(System.in);

    System.out.print("Size of List: ");
    int[] Array1 = createRandArray(inpt.nextInt()); // TODO Create ability to import data e.i Text File, Console Input, etc.

    int response;
    do {
      System.out.println("Menu:\n\t1. 1 Variable Statistics\n\t2. Z-Test\n\t3. Z-Interval\n\t4. T-Test\n\t" +
        "5. T-Interval\n\t6. Chi-Test");
      response = inpt.nextInt();
      switch(response) {
        case 1:
          oneVarStats(Array1);
          break;
        case 2:
          zTest(Array1);
          break;
        case 3:
        default:
          System.out.println("Please select a valid option");
          break;
      }
    } while(response != 0);
  }

  private static int[] createRandArray(int listSize) {
    int[] List1 = new int[listSize];

    System.out.println("Array is being filled with numbers from 1 - 100");
    for(int i = 0; i < List1.length; i++) {
      List1[i] = (int) (Math.random() * (100)) + 1;
    }
    System.out.println("Array is Filled\n");
    return List1;
  }

  private static void oneVarStats(int[] arr) {
    SummaryStatistics sumStats = new SummaryStatistics();
    for(double val: arr) sumStats.addValue(val);
    System.out.println(sumStats.getSummary());
  }

  private static void zTest(int[] arr, int popMean, ) {
    double zScore = ;
    NormalDistribution normDist = new NormalDistribution();
  }
}
